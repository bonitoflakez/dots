Dependencies for DWM

```
sudo pacman -S base-devel libx11 libxft libxineram flameshot
```

Setup DWM


```
git clone https://gitlab.com/bonitoflakez/dots.git

cd dotfiles

cp -r .bin ~/

cp -r .xinitrc ~/

cp -r dunst ~/.config/

cd dwm 
make 
sudo make clean install

cd dmenu 
make 
sudo make clean install

cd st 
make 
sudo make clean install
```
